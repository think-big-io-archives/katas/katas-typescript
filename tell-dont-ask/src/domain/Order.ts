import OrderItem from "./OrderItem";
import OrderStatus from "./OrderStatus";
import Big from "big.js";

export default class Order {
    private _total: Big;
    private _currency: string;
    private _items: OrderItem[];
    private _tax: Big;
    private _status: OrderStatus;
    private _id: number;

    constructor() {
    }


    get total(): Big {
        return this._total;
    }

    set total(value: Big) {
        this._total = value;
    }

    get currency(): string {
        return this._currency;
    }

    set currency(value: string) {
        this._currency = value;
    }

    get items(): OrderItem[] {
        return this._items;
    }

    set items(value: OrderItem[]) {
        this._items = value;
    }

    get tax(): Big {
        return this._tax;
    }

    set tax(value: Big) {
        this._tax = value;
    }

    get status(): OrderStatus {
        return this._status;
    }

    set status(value: OrderStatus) {
        this._status = value;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }
}
